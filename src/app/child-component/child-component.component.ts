import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Example } from './../models/example.model';
import { AppState } from './../app.state';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-child-component',
  templateUrl: './child-component.component.html',
  styleUrls: ['./child-component.component.css']
})
export class ChildComponentComponent implements OnInit, OnDestroy {

  d: Example[];

  private subscriptionList: Subscription[] = [];

  constructor(private store: Store<AppState>) {
    this.subscriptionList.push(
      this.store.select('example').subscribe(example => this.d = example)
    )
  }

  ngOnInit() {
    this.d = null;
  }

  ngOnDestroy() {
    this.subscriptionList.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
  }
}
