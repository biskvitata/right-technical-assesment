import { DataService } from './../services/data.service';
import { StoreModule } from '@ngrx/store';
import { HttpClientModule } from '@angular/common/http';
import { ParentComponentComponent } from './../parent-component/parent-component.component';
import { AppComponent } from './../app.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ChildComponentComponent } from './child-component.component';
import { reducer } from '../reducers/example.reducer';

describe('ChildComponentComponent', () => {
  let component: ChildComponentComponent;
  let fixture: ComponentFixture<ChildComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        ParentComponentComponent,
        ChildComponentComponent
      ],
      imports: [
        HttpClientModule,
        StoreModule.forRoot({
          example: reducer
        }),
      ],
      providers: [
        DataService
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
