import { Action } from '@ngrx/store'

export const SHOW_CHILD = 'SHOW_CHILD'

export class ShowChild implements Action {
    readonly type = SHOW_CHILD

    constructor(public payload: any) {}
}

export type Actions = ShowChild