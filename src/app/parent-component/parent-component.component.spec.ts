import { ChildComponentComponent } from './../child-component/child-component.component';
import { DataService } from './../services/data.service';
import { StoreModule } from '@ngrx/store';
import { HttpClientModule } from '@angular/common/http';
import { ParentComponentComponent } from './../parent-component/parent-component.component';
import { AppComponent } from './../app.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { reducer } from '../reducers/example.reducer';

describe('ParentComponentComponent', () => {
  let component: ParentComponentComponent;
  let fixture: ComponentFixture<ParentComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        ParentComponentComponent,
        ChildComponentComponent
      ],
      imports: [
        HttpClientModule,
        StoreModule.forRoot({
          example: reducer
        }),
      ],
      providers: [
        DataService
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
