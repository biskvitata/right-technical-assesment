import { filter } from 'rxjs/operators';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../services/data.service';

import { Store } from '@ngrx/store';
import { AppState } from './../app.state';
import { Example } from './../models/example.model'
import * as ExampleActions from './../actions/example.actions';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-parent-component',
  templateUrl: './parent-component.component.html',
  styleUrls: ['./parent-component.component.css']
})
export class ParentComponentComponent implements OnInit, OnDestroy {

  data = [];
  child: Example[];

  private subscriptionList: Subscription[] = [];

  constructor(
    private dataService : DataService ,
    private store: Store<AppState>
  ) { }


  ngOnInit() {
    this.subscriptionList.push(
      this.dataService.getJSON().subscribe(data => this.data = data)
    );
  }

  showChild(index) {
    this.child = this.data.filter(x => x.parent === index + 1); 
    this.store.dispatch(new ExampleActions.ShowChild(this.child));
  }
  
  ngOnDestroy() {
    this.subscriptionList.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
  }
}
