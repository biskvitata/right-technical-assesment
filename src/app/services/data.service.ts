import { HttpClient } from '@angular/common/http'; 
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { map, catchError, filter } from 'rxjs/operators'

@Injectable()
export class DataService {
  constructor(private http: HttpClient) {
    this.getJSON().subscribe(data => {
        console.log(data);
    });
  }

  public getJSON(): Observable<any> {
      return this.http
        .get("./assets/mydata.json")
        .pipe(
          map((res:any) => res),
          catchError(err => of(`Error: ${err}`))
        )
  }
}