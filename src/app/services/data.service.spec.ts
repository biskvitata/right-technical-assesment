import { HttpClientModule } from '@angular/common/http';
import { DataService } from './data.service';
import { TestBed, inject } from "@angular/core/testing";

describe("DataService", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DataService
      ],
      imports: [
        HttpClientModule
      ],
    });
  });

  it("should be created", inject([DataService], (service: DataService) => {
    expect(service).toBeTruthy();
  }));
});
