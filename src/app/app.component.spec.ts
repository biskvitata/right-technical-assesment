import { StoreModule } from '@ngrx/store';
import { DataService } from './services/data.service';
import { HttpClientModule } from '@angular/common/http';
import { ParentComponentComponent } from './parent-component/parent-component.component';
import { ChildComponentComponent } from './child-component/child-component.component';
import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { reducer } from './reducers/example.reducer';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        ParentComponentComponent,
        ChildComponentComponent
      ],
      imports: [
        HttpClientModule,
        StoreModule.forRoot({
          example: reducer
        }),
      ],
      providers: [
        DataService
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'technical-task'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('technical-task');
  });

  it('should render title in a h1 tag', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Whole list');
  });
});
