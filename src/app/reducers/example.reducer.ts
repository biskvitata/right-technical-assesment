import { Action, createSelector } from '@ngrx/store'
import { Example } from './../models/example.model'
import * as ExampleActions from './../actions/example.actions'

export function reducer(state: Example[] = [], action: ExampleActions.Actions) {
    switch(action.type) {
        case ExampleActions.SHOW_CHILD:
            return action.payload;
        
        default:
            return state;
    }
}